/*jshint jquery: true, browser: true, devel: true, globalstrict: true */
/*global io: false, fillScores */

'use strict';

$(function () {
    
  var $rightColumn = $('#right-column'),
      $content = $('#content'),
      $tablesList = $('#tables-list');
  
  var $start = $('#start'),
      $result = $('#result');

  var $myScores = $('.p1'),
      $opponentScores = $('.p2');

  var $dices = $('.dice'),
      $change = $('#change');
  
  var selected = 0;
  var gMarks;
  var gMoves;
  
  var gToken = false;

  var socket;

  if (!socket || !socket.connected) {
    socket = io({
      forceNew: true
    });
  }
  
  $change.hide();
  $result.hide();
  $dices.hide();
  
  $rightColumn.hide();
  $content.hide();

  socket.on('connect', function () {
    console.log('Nawiązano połączenie przez Socket.io');
    var nick = $('#nick').text();
    if ('' !== nick) {
      socket.emit('updateNick', nick);
    }
    socket.emit('getTables');
  });
  socket.on('disconnect', function () {
    console.log('Połączenie przez Socket.io zostało zakończone');
  });
  socket.on('error', function (err) {
    console.log('Błąd połączenia z serwerem: "' + JSON.stringify(err) + '"');
  });
  
  
  
//#################################  TABLES  ################################### 
  socket.on('tables', function (tables) {
    $rightColumn.hide();
    $content.hide();
    $('#back').remove();
    $tablesList.show();
    
    renderTables(tables);
  });
  
  socket.on('yourTable', function (name) {
    socket.emit('setTable', name);
    goToTable(name);
  });
  
  socket.on('refreshTablesList', function (tables) {
    if ($tablesList.is(":visible")) {
      renderTables(tables);
    }
  });
  
  socket.on('nicks', function (nicks) {
    var $playersList = $('#players-list');
    $playersList.html('');
    
    var nick;
    for(nick in nicks) {
      if (nicks[nick] === 'active') {
        nick = '<b>' + nick + '</b>';
      }
      $playersList.append('<li class="nick" id="' + nick + '">' + nick +
                          '</li>');
    }
  });
  
  socket.on('message', function (message) {
    var $chat = $('#chat-content');
    $chat.append(message);
    $chat.scrollTop($chat.prop('scrollHeight'));
  });
//##############################################################################

  socket.on('showGame', function () {
    $('#back').remove();
    $start.hide();
    $dices.show();
  });
  
//##################################  GAME  #################################### 
  socket.on('token', function (token) {
    gToken = token;
    if (gToken) {
      socket.emit('getDices');
    }
  });

  socket.on('dices', function (dices, marks, moves, player, token) {
    gToken = token;
    gMarks = marks;
    gMoves = moves;
    dices.forEach(function (elem, idx) {
      var $dice = $('#dice' + (idx + 1));
      $dice.attr('src', 'img/dice_' + elem + '.png');
      $dice.css('opacity', 1);
      $dice.removeAttr('selected');
      selected = 0;
      $change.hide();
    });
    fillScores(marks, player);
  });
  
  socket.on('sums', function (sum1, sum2) {
    $('#p1-sum1').text(sum1);
    $('#p1-sum2').text(sum2);
  });
  
  socket.on('opponentChoice', function (name, score) {
    var selector = '#p2-' + name;
    var $cell = $(selector);
    $cell.text(score);
    $cell.attr('selected', 'selected');
    $cell.css('color', 'black');
    $cell.css('font-weight', 'bold');
    
    $opponentScores.each(function (idx, cell) {
      if (!cell.hasAttribute('selected')) {
        $(cell).text('');
      }
    });
  });
  
  socket.on('opponentSums', function (sum1, sum2) {
    $('#p2-sum1').text(sum1);
    $('#p2-sum2').text(sum2);
  });
  
  socket.on('end', function () {
    $dices.hide();
    $start.html('<span>START</span>');
    $start.css('font-size', '42px');
    
    $start.show();
  });
  
  socket.on('result', function (result) {
    var bgcolor;
    var text;
    var audio;
    if ('win' === result) {
      bgcolor = '#22B14C';
      text = 'WYGRANA';
      audio = new Audio('sounds/win.mp3');
      audio.play();
    } else if ('draw' === result) {
      bgcolor = '#5761D2';
      text = 'REMIS';
    } else {
      bgcolor = '#880015';
      text = 'PRZEGRANA';
      audio = new Audio('sounds/lose.mp3');
      audio.play();
    }
    
    $result.css('background-color', bgcolor);
    $result.html('<span>' + text + '</span>');
    $result.show();
    
    $('body').prepend('<img src="img/back.png" alt="back" id="back">');
    hideResult();
  });
//##############################################################################
  
  
//###############################  INTERFACE  ################################## 
  socket.on('myNick', function(nick) {
    $('#hello').text(nick);
  });
  
  $('body').on('click', '.nick', function () {
    var nick = $(this).text();
    
    if (nick.slice(0, 4) !== 'anon') {
      socket.emit('getStats', nick);
    } else {
      $('#info').remove();
      $('#stat-list').html('');
      $('#sidebar').append(
        '<p id="info">anonimowi gracze nie posiadają statystyk</p>');
      $('#sidebar').animate({
        left: '0'
    }, 300);
    }
  });
  
  socket.on('stats', function(stats) {
    var games = stats.win + stats.draw + stats.lose;
    var winPercentage  = stats.win * 100.0 / games;
    console.log(typeof(winPercentage));
    console.log(stats.win * 100.0 / games);
    $('#info').remove();
    var $list = $('#stat-list');
    $list.html('');
    $list.append('<li>gracz ' + stats.username + '</li>');
    $list.append('<li>rozegrane partie: ' + games + '</li>');
    if (!isNaN(winPercentage)) {
      winPercentage = Math.round(winPercentage);
      $list.append('<li>wygrane: ' + stats.win + ' (' + winPercentage + 
        '%)</li>');
    } else {
      $list.append('<li>wygrane: ' + stats.win + ' (0%)</li>');
    }
    $list.append('<li>zremisowane: ' + stats.draw + '</li>');
    $list.append('<li>przegrane: ' + stats.lose + '</li>');
    $list.append('<li>passa: ' + stats.streak + '</li>');
    $list.append('<li>zdobyte punkty: ' + stats.points + '</li>');
    
    $('#sidebar').animate({
        left: '0'
    }, 300);
  });
  
  $('body').on('click', '#create-table', function () {
    socket.emit('createTable');
  });
  
  $("#sidebar").click(function () {
    $(this).animate({
        left: '-20%'
    }, 300);
  });
  
  $('body').on('click', '.table', function () {
    var $table = $(this);
    var name = $table.attr('id');
    
    if (!$table.hasClass('disabled')) {
      socket.emit('setTable', name);
      goToTable(name);
    }
  });
  
  $('body').on('click', '#back', function () {
    socket.emit('leaveTable');
  });
  
  $('body').on('keyup', '#message', function (event) {
    if (event.keyCode == 13) {
      $("#send").click();
    }
  });
  
  $('body').on('click', '#send', function () {
    var $message = $('#message');
    var text = $message.val();
    if (text !== '') {
      socket.emit('message', text);
      $message.val('');
    }
  });
  
  $start.on('click', function () {
    cleanTable();
    socket.emit('ready');
    $start.html('<span>Oczekiwanie na przeciwnika...</span>');
    $start.css('font-size', '24px');
  });

  $dices.click(function () {
    if (gToken) {
      if (gMoves > 0) {
        var $dice = $(this);

        if (!$dice.attr('selected')) {
          selected++;
          $dice.css('opacity', 0.5);
          $dice.attr('selected', 'selected');
        } else {
          selected--;
          $dice.css('opacity', 1);
          $dice.removeAttr('selected');
        }

        if (selected > 0) {
          $change.show();
        } else {
          $change.hide();
        }
      }
    }
  });

  $change.click(function () {
    if (gToken) {
      var audio = new Audio('sounds/roll.mp3');
      audio.play();
      
      var selected = [];
      var i;
      $dices.each(function (idx, elem) {
        if (elem.hasAttribute('selected')) {
          selected.push(idx);
        }
      });
      socket.emit('change', selected);
    }
  });
  
  $myScores.click(function () {
    if(gToken) {
      var $cell = $(this);

      if(!$cell.attr('selected')) {
        var name = $cell.attr('id').slice(3);
        var score = gMarks[name];

        $cell.text(score);
        $cell.css('color', 'black');
        $cell.css('font-weight', 'bold');
        $cell.attr('selected', 'selected');

        socket.emit('setScore', name, score);

        $myScores.each(function (idx, cell) {
          if (!cell.hasAttribute('selected')) {
            $(cell).text('');
          }
        });
      }
    }
  });
//##############################################################################

  
//###############################  FUNCTIONS  ################################## 
  var cleanTable = function () {
    $myScores.each(function (idx, cell) {
      var $cell = $(cell);
      $cell.text('');
      $cell.removeAttr('selected');
      $cell.css('font-weight', 'normal');
    });
    $opponentScores.each(function (idx, cell) {
      var $cell = $(cell);
      $cell.text('');
      $cell.removeAttr('selected');
      $cell.css('font-weight', 'normal');
    });
    $('#p1-sum1').text(0);
    $('#p1-sum2').text(0);
    $('#p2-sum1').text(0);
    $('#p2-sum2').text(0);
  };
  
  var hideResult = function () {
    setTimeout(function () {
      $result.hide();
    }, 3000);
  };
  
  var goToTable = function (name) {
    $tablesList.hide();
    $rightColumn.css('opacity', 1);
    $content.css('opacity', 1);
    $rightColumn.show();
    cleanTable();
    $content.show();
    $('body').prepend('<img src="img/back.png" alt="back" id="back">');
    $('#table-name').text('Stół #' + (parseInt(name)+1));
    $('#chat-content').html('');
  };
  
  var renderTables = function (tables) {
    $('#create-table').remove();
    $('.table').remove();
    
    var create = '';
    create += '<div id="create-table">';
    create += '<span>nowy stół</span>';
    create += '</div>';
    $tablesList.append(create);
    
    tables.forEach(function (obj, idx) {
      if (obj !== null) {
        var table = '';
        var name = '#' + (idx+1);
        var cls = '';
        if (obj.players === 2) {
          cls = 'disabled';
        }
        table += '<div id="' + idx + '" class="table ' + cls + '">';
        table += '<span>' + name + '</span>';
        table += '</div>';
        $tablesList.append(table);
      }
    });
  };
});