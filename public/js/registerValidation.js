/*jshint jquery: true, browser: true, devel: true, globalstrict: true */
/*global io: false */

'use strict';

$(function () {
  $('form').submit(function (event) {
    var name = $('#username').val(),
        passwd1 = $('#password1').val(),
        passwd2 = $('#password2').val();

    if(name.slice(0, 4) === 'anon') {
      event.preventDefault();
      $('#info').text('Login nie może zaczynać się od "anon".');
      return;
    }
    
    if(!valid(passwd1, passwd2)) {
      event.preventDefault();
      $('#info').text('Podane hasła muszą być jednakowe.');
      return;
    }
  });
  
  var valid = function(passwd1, passwd2) {
    if (passwd1 === passwd2) {
      return true;
    }
    return false;
  };
});