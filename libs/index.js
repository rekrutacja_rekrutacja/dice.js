/*jshint node: true */

exports.rate = function (dices) {
  var count = [0, 0, 0, 0, 0, 0];

  var sum = dices.reduce(function (pre, cur) {
    return pre + cur;
  });

  var marks = {
    d1: 0,
    d2: 0,
    d3: 0,
    d4: 0,
    d5: 0,
    d6: 0,
    sum1: 0,
    x3: 0,
    x4: 0,
    full: 0,
    small: 0,
    big: 0,
    x5: 0,
    chance: 0,
    sum2: 0
  };

  dices.forEach(function (dice) {
    count[dice - 1] += 1;
  });

  marks.d1 = count[0];
  marks.d2 = count[1] * 2;
  marks.d3 = count[2] * 3;
  marks.d4 = count[3] * 4;
  marks.d5 = count[4] * 5;
  marks.d6 = count[5] * 6;

  if (count.indexOf(3) !== -1 || count.indexOf(4) !== -1 || 
      count.indexOf(5) !== -1) {
    marks.x3 = sum;
  }

  if (count.indexOf(4) !== -1 || count.indexOf(5) !== -1) {
    marks.x4 = sum;
  }

  if (count.indexOf(3) !== -1 && count.indexOf(2) !== -1) {
    marks.full = 25;
  }

  if (count[0] > 0 && count[1] > 0 && count[2] > 0 && count[3] > 0 ||
    count[1] > 0 && count[2] > 0 && count[3] > 0 && count[4] ||
    count[2] > 0 && count[3] > 0 && count[4] > 0 && count[5]) {
    marks.small = 30;
  }

  if (count[0] === 1 && count[1] === 1 && count[2] === 1 &&
    count[3] === 1 && count[4] ||
    count[1] === 1 && count[2] === 1 && count[3] === 1 &&
    count[4] === 1 && count[5]) {
    marks.big = 40;
  }

  if (count.indexOf(5) !== -1) {
    marks.x5 = 50;
  }

  marks.chance = sum;


  return marks;
};

exports.sumPoints = function (scores) {
  var sum = 0;
  sum += scores.d1 || 0;
  sum += scores.d2 || 0;
  sum += scores.d3 || 0;
  sum += scores.d4 || 0;
  sum += scores.d5 || 0;
  sum += scores.d6 || 0;
  
  if (sum >= 63) {
    sum += 35;
  }
  
  scores.sum1 = sum;
  
  sum += scores.x3 || 0;
  sum += scores.x4 || 0;
  sum += scores.full || 0;
  sum += scores.small || 0;
  sum += scores.big || 0;
  sum += scores.x5 || 0;
  sum += scores.chance || 0;
  
  scores.sum2 = sum;
};