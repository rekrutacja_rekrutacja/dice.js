/*jshint node: true */

var fs = require('fs');
var wstream = fs.createWriteStream('config/usersdb.js');
var crypto = require('crypto');
var salt = process.env.SALT || 'salt';
var users = [
  {
    username: 'user1',
    password: 'password1',
    win: 8,
    draw: 1,
    lose: 4,
    streak: 2,
    points: 1573
    },
  {
    username: 'user2',
    password: 'password2',
    win: 1,
    draw: 2,
    lose: 7,
    streak: 0,
    points: 933
    }
];

wstream.write('exports.tab = [\n');
users.forEach(function (user) {
  wstream.write('\t{\n');
  wstream.write('\t\t"username" : "' + user.username + '",\n');
  wstream.write('\t\t"password" : "' +
    crypto.pbkdf2Sync(user.password, salt, 4096, 512).toString('hex') + '",\n');
  wstream.write('\t\t"win" : ' + user.win + ',\n');
  wstream.write('\t\t"draw" : ' + user.draw + ',\n');
  wstream.write('\t\t"lose" : ' + user.lose + ',\n');
  wstream.write('\t\t"streak" : ' + user.streak + ',\n');
  wstream.write('\t\t"points" : ' + user.points + ',\n');
  wstream.write('\t},\n');
});
wstream.write('];\n');
wstream.end(function () {
  console.log('initdb done');
});
