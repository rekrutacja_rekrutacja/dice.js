# Dice.js

**Dice.js** is a multiplayer browser dice game.
[Demo on Heroku](https://dicejs.herokuapp.com/)
  
## Installation
To run this game first you have to install its dependencies:
```sh
npm install
bower install
```
Next, you should initialize database:
```sh
npm run initdb
```
Finally, you can run this application
```sh
npm start
```
and play the game [here](http://localhost:3000/).

## Known issues:

App don't act properly when you close game tab or use back navigation button.

## Used technologies

**Dice.js** uses many open source technologies:

* [node.js]
* [jquery]
* [mongodb]
* [ejs]
* [express]
* [mongoose]
* [passport]
* [socket.io]


[node.js]:http://nodejs.org
[jquery]:https://jquery.com/
[mongodb]:https://www.mongodb.org/
[ejs]:http://www.embeddedjs.com/
[express]:http://expressjs.com/
[mongoose]:http://mongoosejs.com/
[passport]:http://passportjs.org/
[socket.io]:http://socket.io/