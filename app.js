/*jshint node: true */

var express = require('express');

var app = express();
var port = process.env.PORT || 3000;
var salt = process.env.SALT || 'salt';
var routes = require('./routes');
var libs = require('./libs');

var server = require('http').Server(app);
var io = require('socket.io')(server);

var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
}));

var session = require('express-session');
var cookieParser = require('cookie-parser');
var flash = require('connect-flash');

app.use(cookieParser('secret'));
app.use(session({cookie: { maxAge: 60000 }}));
app.use(flash());


var crypto = require('crypto');

var mongoose = require('mongoose');
var configDB = require('./config/database');
var User = require('./models/user');
mongoose.connect(configDB.url);
var db = mongoose.connection;
db.on('open', function () {
    console.log('Połączono z MongoDB!');
    var users = require('./config/usersdb');
//    User.remove(); //TODO hę?
    users.tab.forEach(function(user) {
      User.create({
        username: user.username,
        password: user.password,
        win: user.win,
        draw: user.draw,
        lose: user.lose,
        streak: user.streak,
        points: user.points
      });
    });
    console.log('Wypełniono bazę użytkownikami.');
});
db.on('error', console.error.bind(console, 'MongoDb Error: '));

//################################  PASSPORT  ##################################
var passport = require('passport');
var passportLocal = require('passport-local');

app.use(passport.initialize());
app.use(passport.session());

var validateUser = function (username, password, done) {
  User.findOne({
    username: username
  }, function (err, user) {
    if (err) {
      done(err);
    }
    if (user) {
      if (user.password === hash(password)) {
        done(null, user);
      } else {
        done(null, null);
      }
    } else {
      done(null, null);
    }
  });
};

passport.use(new passportLocal.Strategy(validateUser));
passport.serializeUser(function (user, done) {
  done(null, user.id);
});

passport.deserializeUser(function (id, done) {
  User.findOne({
    "_id": id
  }, function (err, user) {
    if (err) {
      done(err);
    }
    if (user) {
      done(null, {
        id: user._id,
        username: user.username,
        password: user.password
      });
    } else {
      done({
        msg: 'Nieznany ID'
      });
    }
  });
});
//##############################################################################

app.set('view engine', 'ejs');
app.set('views', './views');

app.use(express.static('bower_components/jquery/dist'));
app.use(express.static('public'));


//#################################  ROUTES  ###################################
//app.get('/', routes.index);

app.get('/', function (req, res) {
    res.render('index', {
        title: 'Dice.js',
        isAuthenticated: req.isAuthenticated(),
        user: req.user
    });
});

app.get('/login', function (req, res) {
  res.render('login');
  console.log('flash: ' + req.flash('error'));
});

app.post('/login', passport.authenticate('local', {
  failureRedirect: '/login',
  failureFlash: 'Zły login lub hasło'
}), function (req, res) {
  res.redirect('/');
});

app.get('/register', function (req, res) {
  res.render('register');
});

app.post('/register', function (req, res) {
  var name = req.body.username,
      pass = req.body.password1;
  
  User.findOne({
    username: name
  }, function (err, user) {
    if (user) {
      res.redirect('register');
    }
  });
  
  User.create({
    username: name,
    password: hash(pass),
    win: 0,
    draw: 0,
    lose: 0,
    streak: 0,
    points: 0
  }, function (err) {
    if (!err) {
      req.body.password = req.body.password1;
      passport.authenticate('local')(req, res, function () {
        res.redirect('/');
      });
    }
  });
});

app.get('/logout', function (req, res) {
  req.logout();
  res.redirect('/');
});

app.use(routes.noEntry);

server.listen(port, function () {
  console.log('Serwer nasłuchuje na porcie: ' + port);
});
//##############################################################################

var tables = [];

io.on('connection', function (socket) {
  socket.nick = randomNick();
  socket.emit('myNick', 'Zaloguj się, ' + socket.nick + '!'); 
  
  socket.on('updateNick', function(newNick) {
    socket.nick = newNick;
  });

//#################################  TABLES  ###################################
  socket.on('getTables', function () {
    socket.emit('tables', tables);
  });

  socket.on('setTable', function (name) {
    socket.table = tables[name];
    socket.table.name = name;
    socket.join(socket.table.name);
    socket.table.players++;
    socket.scores = {};
    io.emit('refreshTablesList', tables);
    
    socket.table.nicks[socket.nick] = 'inactive';
    io.sockets.in(socket.table.name).emit('nicks', socket.table.nicks);
  });
  
  socket.on('leaveTable', function () {
    delete socket.table.nicks[socket.nick];
    io.sockets.in(socket.table.name).emit('nicks', socket.table.nicks);

    socket.leave(socket.table.name);
    socket.table.players--;
    if (socket.table.player1 && socket.id === socket.table.player1.id) {
      socket.table.player1 = undefined;
      socket.table.ready--;
    } else if (socket.table.player2 && socket.id === socket.table.player2.id) {
      socket.table.player2 = undefined;
      socket.table.ready--;
    }
    if (socket.table.players === 0) {
      tables[socket.table.name] = null;
    }
    delete socket.table;
    socket.emit('end');
    socket.emit('tables', tables);

    io.emit('refreshTablesList', tables);
  });
  
  socket.on('createTable', function () {
    var name;
    var idx = tables.indexOf(null);
    if (idx !== -1) {
      tables[idx] = {players: 0, ready: 0, nicks: {}};
      name = idx;
    } else {
      name = tables.length;
      tables.push({players: 0, ready: 0, nicks: {}});
    }
    socket.emit('yourTable', name);
  });
  
  socket.on('message', function (text) {
    var message = '<b>' + socket.nick + ': </b>' + text + '<br>';
    io.sockets.in(socket.table.name).emit('message', message);
  });
//##############################################################################


//##################################  GAME  ####################################
  socket.on('ready', function () {
    socket.scores = {};
    if (socket.table.player1) {
      socket.table.player2 = {
        id: socket.id,
        nick: socket.nick
      };
    } else {
      socket.table.player1 = {
        id: socket.id,
        nick: socket.nick
      };
    }

    socket.table.ready++;
    if (socket.table.ready === 2) {
      //socket.scores = {};
      socket.table.ready = 0;
      socket.table.moves = 26;

      socket.dices = getDices();
      var marks = libs.rate(socket.dices);
      socket.moves = 2;

      socket.emit('dices', socket.dices, marks, socket.moves, 1, true);
      socket.to(socket.table.name).broadcast.emit('dices', socket.dices, marks,
        0, 2, false);

      socket.emit('showGame');
      socket.to(socket.table.name).broadcast.emit('showGame');
      
      socket.table.nicks[socket.nick] = 'active';
      io.sockets.in(socket.table.name).emit('nicks', socket.table.nicks);
      socket.table.nicks[socket.nick] = 'inactive';
    }
  });

  socket.on('getDices', function () {
    socket.dices = getDices();
    var marks = libs.rate(socket.dices);
    socket.moves = 2;
    socket.emit('dices', socket.dices, marks, socket.moves, 1, true);
    socket.to(socket.table.name).broadcast.emit('dices', socket.dices, marks,
      0, 2, false);
    
    socket.table.nicks[socket.nick] = 'active';
    io.sockets.in(socket.table.name).emit('nicks', socket.table.nicks);
    socket.table.nicks[socket.nick] = 'inactive';
  });

  socket.on('change', function (selected) {
    selected.forEach(function (i) {
      socket.dices[i] = Math.floor((Math.random() * 6) + 1);
    });
    var marks = libs.rate(socket.dices);
    socket.moves--;

    socket.emit('dices', socket.dices, marks, socket.moves, 1, true);
    socket.to(socket.table.name).broadcast.emit('dices', socket.dices, marks,
      0, 2, false);
  });

  socket.on('setScore', function (name, score) {
    socket.scores[name] = score;
    libs.sumPoints(socket.scores);
    socket.emit('sums', socket.scores.sum1, socket.scores.sum2);
    socket.to(socket.table.name).broadcast.emit('opponentSums',
      socket.scores.sum1, socket.scores.sum2);
    socket.to(socket.table.name).broadcast.emit('opponentChoice', name, score);

    if (socket.id === socket.table.player1.id) {
      socket.table.player1.score = socket.scores.sum2;
    } else {
      socket.table.player2.score = socket.scores.sum2;
    }
    
    socket.table.moves--;
    if (socket.table.moves > 0) {      
      socket.emit('token', false);
      socket.to(socket.table.name).broadcast.emit('token', true);
    } else {
      var table = socket.table;
      var nicks = [table.player1.nick, table.player2.nick];
      nicks.forEach(function (nick) {
        if (nick.slice(0, 4) !== 'anon') {
          var score = socket.table.player1.score;
          
          var player,
              opponent,
              p1 = socket.table.player1,
              p2 = socket.table.player2;
          
          if (p1.nick === nick) {
            player = p1;
            opponent = p2;
          } else {
            player = p2;
            opponent = p1;
          }
          
          if (player.score > opponent.score) {
            player.win = 1;
            player.draw = 0;
            player.lose = 0;
            player.streak = 1;
          } else if (player.score === opponent.score) {
            player.win = 0;
            player.draw = 1;
            player.lose = 0;
            player.streak = 0;
          } else {
            player.win = 0;
            player.draw = 0;
            player.lose = 1;
            player.streak = 0;
          }
          
          User.findOne({
            username: nick
          }, function (err, user) {
            if (user) {
              var win = parseInt(user.win);
              var draw = parseInt(user.draw);
              var lose = parseInt(user.lose);
              var streak = parseInt(user.streak);
              var points = parseInt(user.points);
              
              if (player.streak === 0) {
                streak = 0;
              } else {
                streak++;
              }

              user.win = win + player.win;
              user.draw = draw + player.draw;
              user.lose = lose + player.lose;
              user.streak = streak;
              user.points = points + player.score;
              user.save();
            } 
          });
        }
      });
      socket.emit('end');
      socket.to(socket.table.name).broadcast.emit('end');

      result(socket);
      
      delete socket.table.player1;
      delete socket.table.player2;
    }
  });
  
  socket.on('getStats', function (nick) {
    User.findOne({
      username: nick
    }, function (err, user) {
      if (user) {
        socket.emit('stats', user);
      }
    });
  });
});
//##############################################################################

//###############################  FUNCTIONS  ##################################
var getDices = function () {
  var numbers = [];
  var length = 5;

  var i;
  for (i = 0; i < length; i++) {
    numbers.push(Math.floor((Math.random() * 6) + 1));
  }

  return numbers;
};

var result = function (socket) {
  var p1 = socket.table.player1,
      p2 = socket.table.player2;

  if (p1.score > p2.score) {
    if (p1.id === socket.id) {
      socket.emit('result', 'win');
      socket.broadcast.to(p2.id).emit('result', 'lose');
    } else {
      socket.emit('result', 'lose');
      socket.broadcast.to(p1.id).emit('result', 'win');
    }
  } else if (p1.score === p2.score) {
    socket.emit('result', 'draw');
    socket.broadcast.emit('result', 'draw');
  } else {
    if (p1.id === socket.id) {
      socket.emit('result', 'lose');
      socket.broadcast.to(p2.id).emit('result', 'win');
    } else {
      socket.emit('result', 'win');
      socket.broadcast.to(p1.id).emit('result', 'lose');
    }
  }
};

var randomNick = function () {
  var numbers = [];
  var length = 5;

  var i;
  for (i = 0; i < length; i++) {
    numbers.push(Math.floor(Math.random() * 10));
  }
  
  return 'anon' + numbers.join('');
};

var hash = function (password) {
  return crypto.pbkdf2Sync(password, salt, 4096, 512).toString('hex');
};
//##############################################################################